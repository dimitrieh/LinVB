# Alternative versifications

This folder contains 3 news [alternative versification](https://crosswire.org/wiki/Alternate_Versification) schemes for liturgical variants of Catholic Bibles:

- **canon_catholic_lit.h** : The same as *canon_catholic.h* (Esther with 10 chapters) but with some corrections.
- **canon_catholic2_lit.h** : The same as *canon_catholic2.h* (Esther with 16 chapters) but with some corrections.
- **canon_catholic3_lit.h** : The same as *canon_catholic_lit.h* (Esther with 10 chapters) but the alternate versification is merged to the 10 chapters of Esther. This canon allows to better integrate the Greek Esther passages usually numbered with letters (A, B, C, D, E, F) all the verses of these parts are merged to the 10 chapters.
