1,1.	Awa tobandi lisolo lya Neemia, lokola ye moko akomi lyango.
1,1.	Artaksasta wa 1 akomaki moko­nzi o 465 liboso lya Y.K..
1,3.	Balendisi bifelo na bizibeli biye bato ba Babilon babebisaki o 587. Nta­ngo bautaki o boombo bameki kobongisa byango, kasi ba-Samaria bapekisi ba­ngo, ata mpe o ebandela ya bokonzi bwa Artaksasta naino (Esd 4,6-7.23).
1,11.	‘moto oyo’, nde mokonzi ; Neemia azalaki molaki wa ye.
2,6.	Mibu zomi (445 tee 443) Neemia azalaki wana te (13,6) ; mbele alobi yango awa mpo ya bokei bwa mwa mikolo.
2,10.	Sambalat na Tobia bazalaki ba-ntoma ba mokonzi wa Persi, kasi bazalaki kondimela ba-Samaria na ba-Amon.
2,13.	Neemia ameki kotambola zongazonga na engumba, kasi akoki te mpo ya biteni bya efelo bilali wana kilikili.
2,19.	Banguna ba Neemia bameki kobangisa ye, yango wana bayebisi ye ’te : « ba-Persi bakokanisa ’te bokotombokela bango, mpo bolingi kotonga bifelo bya Yeruzalem. »
3,1.	Neemia alendisi bato banso mpo ’te bamipesa na mosala mwa botongi ; banso bandimi.
5,1.	Mbele makambo ma mpasi ma­kweli bango : to babuki mbuma mingi te, to banguna bakoteli bango. Bato mingi bapesaki biloko to bana ba bango bo ndanga. Neemia asali makasi mpo ’te banso bazwa biloko bya bango, mpe bapesa baombo bonsomi lisusu mpo ya kolendisa boyokani o kati ya bato. Alobi ’te ye moko alukaki ata mbala yoko bobele bolamu bwa ye moko te, ntango bandeko ba ye bakomaki na mpasi.
8,9.	Mbele babakisi nkombo ya Neemia bobele na nsima, mpo balingaki kotala ye lokola Esdra : ye moto asalaki ’te Mobeko mondimama na bakonzi ba leta. Kasi amilakisi ata moke te o ntango ya milulu myango.
8,14.	Tala Lv 23,42.
9,2.	Tala Esd 9,1-2 ; 10,11. Lisumu linene basali : ndenge basali bampaya malamu te.
10,1.	Nsima ya litangi lya Mobeko banso bandimi kotosa makambo maye : babala lisusu basi bampaya te mpe batosa mobeko mwa Sabato.
11,2.	Ba-Yuda baike balingaki kofanda o mboka pene na bilanga bya bango, kasi o Yeruzalem te, mpo ndako mingi ibebaki.
12,27.	Lisolo lya molulu moye lisengeli kotangema nsima ya 6,16 ; mbele mokomi alingi koyebisa liboso ’te Yeruzalem ekomi na bato mwa mingi, Mobeko mondimami bo mobeko mwa leta, mpe ndenge Esdra apetoli Yeruzalem.
12,31.	Bifelo biye bizingi Yeruzalem bizalaki na bolai bwa metele 6 tee 10 ; nsonge ya byango ezalaki mwa enene ; yango elingisi bango kotambola wana na bato baike.
12,36.	Mokomi abakisi nkombo ya Esdra awa (tala 8,9). Mbele azalaki o Yeruzalem te o ntango ena. Abakisi nkombo ya ye, mpo bango babale basali mosala se moko moye mopesi bango babale lokumu.
13,2.	Tala Mbk 23,4-6.
13,7.	Toyebi te mikolo boni aumeli sikawa o Yeruzalem. Ekomi ya kuna, amoni ’te bazalaki kotosa bitinda bisusu bya Mobeko te ; na mpiko enene akopalela bango.
13,10.	O 12,44-47 totangi ’te bato bazalaki kopesa biloko na motema likabo ; kasi awa tokoki kotanga ’te bazalaki kosala bongo lisusu te.
13,19.	Sabato ezalaki kobanda na mpokwa ya mokolo mwa motoba. Tala mpe ndimbola ya Sabato o Bob 20,8-11 ; Mit 15,32-36 ; Mbk 5,12-15 ; Yer 17,19-27.
13,24.	Bana baye bazalaki koloba lokota la ba-Aram ; bato bazalaki koloba lokota la ebrei bazalaki lisusu mingi te.