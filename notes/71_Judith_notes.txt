1,1.	Buku eye ekomami o ntango bayebi lisusu makambo ma mokonzi wa Babilon malamu te, kasi babosani te ’te mobotoli oyo azalaki na makambo makasi makasi, mpe Ninive engumba ya lolendo.
1,5.	Ragau ezalaki o Nordi-Esti ya Ekbatane (km. 160).
1,6.	Nkombo esusu ya ba-Kaldea : Nabukonozor na bato ba ye.
1,11.	Bato baye bakotombokela bato ba Babilon, mpo bazalaki konyokolo bango mingi mpe kofutisa bango mpako enene.
2,21.	Mokomi ayebi bisika bya ekolo eye malamu mingi te : na nzela eye alakisi basoda ba Asur bazongeli bisika biye basili balekaki.
3,8.	Nabukodonozor alingi ’te batika ko­kumisa banzambe banso mpo bakumisa bobele ye moko. Ba-Yuda bakoki kowangana Yawe te, mpo azali Nzambe wa solo oyo amilakisi mbala mingi na ba­nkoko mpe akati bondeko na bango (4,2).
4,6.	O bisika nini bingumba biye bizalaki eyebani lisusu te, kasi mokomi ayebisi ’te bizalaki o nzela eye Oloferne akolanda mpo akoma o ngomba ya Yudea.
5,5.	Mopagano oyo Akior azalaki moto malamu mpe wa bwanya ; ayebi mikóló mya ba-Yuda malamu na mpe maye matali bondeko boye Nzambe akati na bango. Na nsima ye mpe akokumisa Yawe (14,10).
7,31.	Ata atii motema na Nzambe, Ozia atikali na mwa ntembe. Kasi Yudite atii motema monso na Nzambe.
8,1.	‘Yudite’ = ‘mwasi mo-Yuda’ ; azali lokola elembo ya boyambi mpe ya mpiko ya ekolo ya ye mobimba.
8,5.	Yudite alingaki kokende wana mpo ya kosambela, lokola profeta Elia (2 Bak 4,8-10).
8,24.	Yudite ayebisi bato ba Betulia ’te basengeli kotia mitema bobele na Nza­mbe. Soko babwaki elikya, nzela ya kokende o Yeruzalem ekokoma polele mpe banguna bakobotolo engumba : yango ekoyokisa bango nsoni mpenza.
8,25.	Yudite ayebisi bango ’te maka­mbo maye ma mpasi mazali etumbu te, lokola bango bakanisi : makweli bango mpo ya kokolisa boyambi bwa bango.
8,33.	Akoya kotala bango, to mpo asala bango malamu (lokola o esika eye), to mpo atumbola bango mpo basalaki mabe.
9,2.	Ezali likambo lya Dina, elenge mwasi oyo Sikem abebisaki ; bandeko ba Dina bazongiseli ye mabe mana (Lib 34).
10,5.	Yudite akei na biloko bya ye bya kolia, mpo aboyi kolia biloko biye ba­mpaya balambi, ata epekisami na mobeko mwa ba-Yuda te.
10,10.	Balandi ye na miso mpo bamituni nini Yudite alingi kosala, mpe soko bakomono ye lisusu nzoto kolongonu.
11,5.	Maye Yudite alobi ekoki koyokana na ndenge ibale : Oloferne akoyoka mango lokola makosepelisa ye, kasi Yudite ayebisi na bonkutu ’te maye akosala makobongela ba-Asur te. Soko akosi bongo, ezalaki mpo asengeli kosala mayele mpo ’te bakoka kobika o maboko ma banguna.
11,6.	Oloferne akanisi ’te Nzambe akosalisa ye o maye akosala, kasi ekosukela ye mabe (tala mpe 11,16) : ntango Yudite alobi « mokonzi wa ngai », Oloferne akanisi ’te azali koloba mpo ya ye (11,5), kasi mbala eye Yudite alakisi Nzambe na maloba maye.
11,17.	Yudite alengeli bongo ndenge akokima nganda ya banguna.
12,18.	Oloferne akanisi ’te Yudite asepeli mingi mpo aponi ye, nzokande asepeli na ndenge alingi abikisa ekolo ya ye.
13,15.	Se lokola Yael, mwasi wa Eber, oyo abomaki Sisera (Baz 4,7-21 ; 5,24-27) to mwasi wa Tebes oyo abomaki Abimelek (Baz 9,53-54).