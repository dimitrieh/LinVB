1,1.	Mokomi ayebisi ndenge Yeruzalem ekomi na mpasi mpe bato ba Yeruzalem bakambami o boombo. Balakisi Yeruzalem lokola mwasi bozenge oyo azali komilela mpe kondima mabe ma ye ; abondeli Nzambe aya kosalisa ye mpe atumbola banyokoli ba ye.
1,2.	Bata ba bikolo bya penepene baye balakelaki bato ba Yeruzalem lisalisi o etumba na Babilon, kasi batikaki bango (Yer 4,30 ; Ez 16,37-40 ; 23,22-29).
1,15.	Mai matane ma mbuma ya vino o ekamelo mazali komonono lokola makila. Bato ba Babilon basopaki makila ma bato ba Yeruzalem, lokola bakamolaki bango o ekamelo.
1,19.	Tala 1,2... .
2,1.	–Esilisi mokomi kolakisa maye makweli bakonzi, banganga Nzambe, baprofeta, bakolo na bana (1-12), akebisi Sion (13-17), akundoleli bato ba ye maloba ma bokosi ma baprofeta ba lokuta, mpe asengi bango bamilela.
	–Ebonga eye ezali Tempelo : tala Ez 43,7 ; Nz 99,5 ; 132,7.
3,1.	Liboso mokomi amileli mpo ya mpasi inso ikweli ye (1-21). Na nsima apesi malako ma bato ba bwanya matali mpasi ikoki kokwela bato mpe bolamu bwa Nzambe (22-39), mpe ale­ndisi bandeko ba ye mpo ’te bandima ’te basali masumu mpe bazongela Nza­mbe (40-47) ; na nsuka amileli lisusu (48-66). Mbele na bomileli boye akanisi mpasi ya bato banso.
4,1.	Mpasi enene ekweli bato banso : bana bake bazali kokufa na nzala (3-4), bato ba nkita bakomi babola mpenza (5), bilenge babali bakomi kozanga bokasi (7-8), basi basusu balei bana ba bango (10). Solo mpasi ekweli bato ba­nso, ná banganga Nzambe ná baprofeta ná bakolo ná mokonzi wa ekolo (13-20). Nzokande Nzambe akolimbisa mpe akobongisa bango (22).
4,3.	Maligbanga mazali ndeke ilai, leka bato ; iyebi kopimbwa te, kasi ikotambolaka mbangu mpenza. Liboso bazalaki kokanisa ’te maligbanga izalaki kolinga bana ba yango te, mpo ikokundaka makei ma yango o nse ya zelo ; nzokande ikofandelaka makei te, mpo moi makasi mokosalaka ’te makei mafungwana. Awa balakisi bato lokola maligbanga mpo bato mingi batiki bana ba bango ntango mpasi enene ekweli bango.
4,15.	Nzoto ya bato ba Yeruzalem etondi na makila ma mpota ; bakomi bongo lokola bato ba maba, baye bakoki kokoma pene na bato basusu te (Lv 13,45-46).
4,21.	Ba-Edom, baye bazalaki koyina ba-Israel, basepelaki na bomoni ’te Yeruzalem ebebi ; o ntango ena babotoloki ndambo ya mabelé ma bango (2 Bak 24,2 ; Ez 25,12-14 ; 35,1-15).
5,1.	Nzembo eye ezali losambo : basengi Nzambe abatela bato ba ye, mpe atala mpasi inso bazali koyoka.
5,8.	Bankumu ba bango moko bazalaki koyangela bango lisusu te ; base­ngelaki kotosa mokonzi wa Babilon na basaleli ba ye, baye batangemi na bokinoli ‘baombo’ awa.
5,13.	Mabanga ma konika mbuma ya mampa mazali bozito mingi ; ata bato bazali na makasi bakoki kokumba mango bobele na mpasi.
5,19.	Nkembo na nguya ya Nzambe ibongwanaka te, ata bato ba ye bakomi na mpasi enene.